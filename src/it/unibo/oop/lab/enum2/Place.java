package it.unibo.oop.lab.enum2;

/**
 * Represents and enumeration for the places {@link Sport} are played
 *
 */

public enum Place {
	INDOOR, OUTDOOR;
}
