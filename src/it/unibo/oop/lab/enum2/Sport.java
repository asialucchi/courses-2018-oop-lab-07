/**
 * 
 */
package it.unibo.oop.lab.enum2;

/**
 * Represents an enumeration for declaring sports.
 * 
 * 1) You must add a field keeping track of the number of members each team is
 * composed of (1 for individual sports)
 * 
 * 2) A second field will keep track of the name of the sport.
 * 
 * 3) A third field, of type Place, will allow to define if the sport is
 * practiced indoor or outdoor
 * 
 */

public enum Sport {
	
	BASKET (5, "Basket", Place.INDOOR),
	VOLLEY (6, "Volley", Place.INDOOR),
	TENNIS (2, "Tennis", Place.OUTDOOR),
	BIKE (1, "Bike", Place.OUTDOOR),
	F1 (1, "F1", Place.OUTDOOR),
	MOTOGP (1, "MotoGp", Place.OUTDOOR),
	SOCCER (10, "Soccer", Place.OUTDOOR);
	
    /*
     * TODO
     * 
     * Declare the following sports:
     * 
     * - basket
     * 
     * - volley
     * 
     * - tennis
     * 
     * - bike
     * 
     * - F1
     * 
     * - motogp
     */

    /*
     * TODO
     * 
     * [FIELDS]
     * 
     * Declare required fields
     */
	private final int membri;
	private final String name;
	private final Place where;
    /*
     * TODO
     * 
     * [CONSTRUCTOR]
     * 
     * Define a constructor like this:
     * 
     * - Sport(final Place place, final int noTeamMembers, final String actualName)
     */
	/**
	 * 
	 * @param membri
	 * 		number of people that play the sport
	 * @param name
	 * 		name of the sport
	 * @param where
	 * 		where the sport is played
	 */
	private Sport (final int membri, final String name, final Place where) {
		this.membri = membri;
		this.name = name;
		this.where = where;
	}
    /*
     * TODO
     * 
     * [METHODS] To be defined
     * 
     * 
     * 1) public boolean isIndividualSport()
     * 
     * Must return true only if called on individual sports
     * 
     * 
     * 2) public boolean isIndoorSport()
     * 
     * Must return true in case the sport is practices indoor
     * 
     * 
     * 3) public Place getPlace()
     * 
     * Must return the place where this sport is practiced
     * 
     * 
     * 4) public String toString()
     * 
     * Returns the string representation of a sport
     */
	/**
	 * 
	 * @return true
	 * 		if the sport is individual
	 */
	
	public boolean isIndividualSport() {
		return this.membri == 1;
	}
	
	/**
	 * 
	 * @return true
	 * 		if the sport is played indoor
	 */
	public boolean isIndoorSport() {
		return this.where == Place.INDOOR;
	}
	
	/**
	 * 
	 * @return where the place is played
	 */
	public Place getPlace() {
		return this.where;
	}
	
	public String toString() {
		return this.name();
	}
}
	
