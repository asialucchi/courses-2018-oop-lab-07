package it.unibo.oop.lab.nesting2;

import java.util.Iterator;
import java.util.List;
import it.unibo.oop.lab.nesting2.Acceptor;
import it.unibo.oop.lab.nesting2.Acceptor.ElementNotAcceptedException;

public class OneListAcceptable<T> implements Acceptable<T>{

	private List<T> lista;
	
	public OneListAcceptable(final List<T> lista) {
		this.lista = lista;
	}

	private class AcceptorImpl<T> implements Acceptor<T>{
		
		private final Iterator<T> it = (Iterator<T>) lista.iterator();

		@Override
		public void accept(T newElement) throws ElementNotAcceptedException {
			if (it.next()!=newElement) {
				throw new ElementNotAcceptedException(newElement);
			} 
		}

		@Override
		public void end() throws EndNotAcceptedException {
			if (it.hasNext()) {
				throw new EndNotAcceptedException();
			}
		}
		
	}
	
	@Override
	public Acceptor<T> acceptor() {
		return new AcceptorImpl<T>();
	}
}
